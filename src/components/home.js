import React, {Component} from 'react';
import './home.css';
import { Controls, PlayState, Tween } from 'react-gsap';
import { TweenMax } from "gsap";
import hoverEffect from 'hover-effect'
export default class Home extends Component {
    componentDidMount = () => {
        new hoverEffect({
            parent: document.querySelector('.distortion'),
            intensity: 0.2,
            image1: require('../img/01.png'),
            image2: require('../img/02.png'),
            displacementImage: require('../img/diss.png'),
            imagesRatio: 380 / 300
          });
    }
    render() {
       
        return (
            <div className="wrapper">
                 <Tween
                    to={{ top: "-100%"}}
                    duration={1.5}
                    delay={0.5}
                    ease="back.out(1.5)"
                >
                    <div className="overlay first"></div>
                </Tween>
                <Tween
                    to={{ top: "-100%"}}
                    duration={1.5}
                    delay={0.7}
                    ease="back.out(1.5)"
                >
                    <div className="overlay second"></div>
                </Tween>
                <Tween
                    to={{ top: "-100%"}}
                    duration={1.5}
                    delay={0.9}
                    ease="back.out(1.5)"
                >
                    <div className="overlay third"></div>
                </Tween>
               
                <nav className="navbar">
                    <Tween
                    delay={1.5}
                    duration={1.5}
                    stagger = {0.08}
                    from= {{opacity: '0',   y: "20"}}
                    ease="back.out(1.5)"
                    >
                    <div class="menu">
                        <ion-icon name="ios-menu"></ion-icon>
                    </div>
                    <div class="lang">eng</div>
                    <div class="search">
                        <ion-icon name="ios-search"></ion-icon>
                    </div>
                    </Tween>
                </nav>

                <div class="media">
                    <ul>
                        <Tween
                         delay={1.5}
                         duration={1.5}
                         stagger = {0.08}
                         from= {{opacity: '0',   x: "-20"}}
                         ease="back.out(1.5)"
                        >
                        <li>facebook</li>
                        <li>instagram</li>
                        <li>twitter</li>
                        </Tween>
                    </ul>
                </div>

                <div class="text">
                    <h1>
                        <Tween
                         delay={1}
                         duration={1.5}
                         from = {{y: '100%'}}
                        >
                        <span class="hidetext">toni&guy</span>
                        </Tween>
                    </h1>
                    <Tween
                    delay={1.5}
                    duration={1.5}
                    from = {{opacity: '0', x:'-10000'}}
                    >
                    <h2>duality</h2>
                    </Tween>
                    <h3>
                        <Tween
                         delay={1.2}
                         duration={1.5}
                         from= {{y: '100%'}}
                         >
                        <span class="hidetext">collection 2017 <br/> duality</span>
                        </Tween>
                    </h3>
                    <p>
                        <Tween
                         delay={1.3}
                         duration={1.5}
                         from= {{y: '100%'}}
                        >
                        <span class="hidetext">
                        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Unde quis, delectus facere
                        neque sunt commodi quae
                        culpa dolores doloribus magnam?
                        </span>
                        </Tween>
                    </p>
                    </div>

                    <div class="sponsor">
                        <Tween
                         delay={1.5}
                         duration={1.5}
                         from= {{y: '20', opacity: '0'}}
                        >
                        <img src={require('../img/sponsor-logo.png')} alt=""></img>
                        </Tween>
                        <Tween
                         delay={1.6}
                         duration={1.5}
                         from= {{y: '20', opacity: '0'}}
                        >
                        <p>official sponsor</p>
                        </Tween>
                    </div>
                    <Tween
                     delay={2}
                     duration={1.5}
                     from= {{y: '20', opacity: '0'}}
                    >
                    <div class="distortion"></div>
                    </Tween>
               
            </div>
        )
    }
}